FROM node:14-alpine
RUN npm install -g serverless
RUN npm install -g serverless-offline
WORKDIR /usr/app
COPY package*.json ./
RUN yarn
COPY serverless.yaml .
COPY handler.js .
EXPOSE 3000
ENTRYPOINT [ "sls" ]
CMD ["offline", "start" ]