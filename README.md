# Fuzzing a serverless app in CI with Gitlab

* `kaniko`: builds project container image
* `fuzz:teatime` fuzzes the built image at every commit, with a timeout of 15 minutes
* `fuzz:bedtime` fuzzes the built image on scheduled jobs (here a pipeline is scheduled to start every day at 1800), with a timeout of 8h

### test locally
```bash
# install deps
npm install -g serverless
npm install -g serverless-offline
# deploy with serverless
sls offline start
curl http://localhost:3000/dev/hello-world
```
### Useful resources when using lambda, node and serverless

* https://sls.zone/pages/learn/developing-locally-with-serverless-offline/
* https://docs.aws.amazon.com/apigateway/latest/developerguide/api-gateway-create-api-as-simple-proxy-for-lambda.html
* https://dashbird.io/blog/how-to-deploy-nodejs-application-aws-lambda/
* https://linuxize.com/post/curl-post-request/
* https://stackify.com/aws-lambda-with-node-js-a-complete-getting-started-guide/
* https://stackoverflow.com/questions/4870328/read-environment-variables-in-node-js
