'use strict';

module.exports.hello = async (event) => {
  return {
    statusCode: 200,
    body: JSON.stringify(
      {
        message: 'Go Serverless v1.0! Your function executed successfully!',
        input: event,
      },
      null,
      2
    ),
  };
};

module.exports.ping = async () => {
  return {
    statusCode: 200,
    body: JSON.stringify(
      {
        message: 'Pong!'
      },
      null,
      2
    ),
  };
};

module.exports.commit = async () => {
  return {
    statusCode: 200,
    body: JSON.stringify(
      {
        message: process.env.CI_COMMIT_SHORT_SHA
      },
      null,
      2
    ),
  };
};

module.exports.plusone = async (event) => {
  let body = JSON.parse(event.body)
  return {
    statusCode: 200,
    body: JSON.stringify(
      {
        message: body.number += 1
      },
      null,
      2
    ),
  };
};
